ARROW_DOWN = "arrow_down"
ARROW_LEFT = "arrow_left"
ARROW_RIGHT = "arrow_right"
ARROW_UP = "arrow_up"
A_B = "A-B"
BASS_DOWN = "bass_down"
BASS_RESET = "bass_reset"
BASS_UP = "bass_up"
BLUETOOTH_PAIR = "bluetooth_pair"
CHANNEL_DOWN = "channel_down"
CHANNEL_LIST = "channel_list"
CHANNEL_UP = "channel_up"
CLOCK = "clock"
COLOR_BLUE = "blue"
COLOR_GREEN = "green"
COLOR_RED = "red"
COLOR_YELLOW = "yellow"
DIMMER = "dimmer"
EQUALIZER = "equalizer"
EXIT = "exit"
GUIDE = "guide"
HOME = "home"
INFO = "info"
INTRO = "intro"
MANUAL = "manual"
NUM_0 = "0"
NUM_1 = "1"
NUM_2 = "2"
NUM_3 = "3"
NUM_4 = "4"
NUM_5 = "5"
NUM_6 = "6"
NUM_7 = "7"
NUM_8 = "8"
NUM_9 = "9"
OK = "ok"
OPEN_DISC = "open_disc"
PAUSE = "pause"
PICTURE_MODE = "picture_mode"
PICTURE_SIZE = "picture_size"
PLAY = "play"
POWER = "power"
PREVIOUS_CHANNEL = "previous_channel"
PROG = "prog"
REPEAT = "repeat"
RETURN = "return"
SETTINGS = "settings"
SHUFFLE = "shuffle"
SKIP_BACK = "skip_back"
SKIP_FORWARD = "skip_forward"
SLEEP = "sleep"
SOUND_MODE = "sound_mode"
SOURCE = "source"
SOURCE_AUX = "source_aux"
SOURCE_BLUETOOTH = "source_bluetooth"
SOURCE_CD = "source_cd"
SOURCE_SD = "source_sd"
SOURCE_TUNER = "source_tuner"
SOURCE_USB = "source_usb"
STOP = "stop"
SUBTITLE = "subtitle"
TELETEXT = "teletext"
TIMER = "timer"
TOOLS = "tools"
VOLUME_DOWN = "volume_down"
VOLUME_MUTE = "volume_mute"
VOLUME_UP = "volume_up"


class Remote:

    def __init__(self, code_length: int, codes: dict, has_id_extra=False, id_length=4, data_length=4):
        self.has_id_extra = has_id_extra
        self.code_length = code_length
        self.codes = codes
        self.id_length = id_length
        self.data_length = data_length

    def decode(self, encoded):
        if type(encoded) == int:
            return ('{' + (':#0{}'.format(self.code_length + 2)) + 'X}').format(encoded), self.codes.get(
                encoded)

    def encode(self, decoded):
        for key, value in self.codes.items():
            if decoded == value:
                return key
            return 0


SAMSUNG_TV_REMOTE = Remote(has_id_extra=False, code_length=8, codes={
    0xe0e040bf: POWER,
    0xe0e08877: NUM_0,
    0xe0e020df: NUM_1,
    0xe0e0a05f: NUM_2,
    0xe0e0609f: NUM_3,
    0xe0e010ef: NUM_4,
    0xe0e0906f: NUM_5,
    0xe0e050af: NUM_6,
    0xe0e030cf: NUM_7,
    0xe0e0b04f: NUM_8,
    0xe0e0708f: NUM_9,
    0xe0e036c9: COLOR_RED,
    0xe0e028d7: COLOR_GREEN,
    0xe0e0a857: COLOR_YELLOW,
    0xe0e06897: COLOR_BLUE,
    0xe0e0e01f: VOLUME_UP,
    0xe0e0d02f: VOLUME_DOWN,
    0xe0e0f00f: VOLUME_MUTE,
    0xe0e048b7: CHANNEL_UP,
    0xe0e008f7: CHANNEL_DOWN,
    0xe0e0807f: SOURCE,
    0xe0e034cb: TELETEXT,
    0xe0e0c837: PREVIOUS_CHANNEL,
    0xe0e0d629: CHANNEL_LIST,
    0xe0e058a7: SETTINGS,
    0xe0e09e61: HOME,
    0xe0e0f20d: GUIDE,
    0xe0e0fc03: MANUAL,
    0xe0e0f807: INFO,
    0xe0e006f9: ARROW_UP,
    0xe0e0a659: ARROW_LEFT,
    0xe0e046b9: ARROW_RIGHT,
    0xe0e08679: ARROW_DOWN,
    0xe0e016e9: OK,
    0xe0e01ae5: RETURN,
    0xe0e0b44b: EXIT,
    0xe0e07c83: PICTURE_SIZE,
    0xe0e014eb: PICTURE_MODE,
    0xe0e0a45b: SUBTITLE,
    0xe0e0629d: STOP,
    0xe0e0a25d: SKIP_BACK,
    0xe0e012ed: SKIP_FORWARD,
    0xe0e0e21d: PLAY,
    0xe0e052ad: PAUSE,
    0xe0e0d22d: TOOLS

})

SAMSUNG_SB_REMOTE = Remote(has_id_extra=True, code_length=9, codes={
    0x0cf000ef1: POWER,
    0x0cf00ae51: SOURCE,
    0x0cf00ed12: BLUETOOTH_PAIR,
    0x0cf002dd2: ARROW_UP,
    0x0cf000df2: ARROW_LEFT,
    0x0cf008d72: ARROW_RIGHT,
    0x0cf00ad52: ARROW_DOWN,
    0x0cf0051ae: OK,
    0x0cf00ee11: VOLUME_UP,
    0x0cf001ee1: VOLUME_DOWN,
    0x0cf008e71: VOLUME_MUTE,
    0x0cf00de21: SOUND_MODE,
    0x0cf0041be: BASS_UP,
    0x0cf00c13e: BASS_DOWN,
    0x0cf005ea1: BASS_RESET,
    0x0cf00659a: SETTINGS,
})

TERRIS_STEREO_REMOTE = Remote(has_id_extra=False, code_length=8, codes={
    0X1725E31C: POWER,
    0X1725C936: OPEN_DISC,
    0X17258D72: SOURCE_CD,
    0X17254DB2: SOURCE_USB,
    0X17251DE2: SOURCE_SD,
    0X17252DD2: SOURCE_TUNER,
    0X1725ED12: SOURCE_AUX,
    0X17259D62: SOURCE_BLUETOOTH,
    0X172551AE: BLUETOOTH_PAIR,
    0X17256996: DIMMER,
    0X172509F6: INTRO,
    0X172515EA: REPEAT,
    0X1725D52A: A_B,
    0X1725956A: SHUFFLE,
    0X17258976: SLEEP,
    0X172549B6: TIMER,
    0x1725916e: BASS_DOWN,
    0X172511EE: BASS_UP,
    0X1725A55A: INFO,
    0X172521DE: EQUALIZER,
    0X172559A6: ARROW_UP,
    0X1725CE31: ARROW_LEFT,
    0X17254EB1: ARROW_RIGHT,
    0X1725D926: ARROW_DOWN,
    0X172505FA: OK,
    0X17258E71: STOP,
    0X1725A15E: VOLUME_MUTE,
    0X1725AE51: SKIP_FORWARD,
    0X17252ED1: SKIP_BACK,
    0X17257E81: PLAY,
    0X1725FE01: PAUSE,
    0X172501FE: VOLUME_UP,
    0X1725817E: VOLUME_DOWN,
    0X172506F9: NUM_0,
    0X17258679: NUM_1,
    0X172546B9: NUM_2,
    0X1725C639: NUM_3,
    0X172526D9: NUM_4,
    0X1725A659: NUM_5,
    0X17256699: NUM_6,
    0X1725E619: NUM_7,
    0X172516E9: NUM_8,
    0X17259669: NUM_9,
    0X17255EA1: CLOCK,
    0X1725E51A: PROG
})
