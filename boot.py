# Complete project details at https://RandomNerdTutorials.com


import gc

import esp
import network

esp.osdebug(None)

gc.collect()

ssid = 'Wifi-Name'
password = 'Wifi-Pass'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while not station.isconnected():
    pass

print('Connection successful')
print(station.ifconfig())

