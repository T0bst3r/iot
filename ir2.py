import machine
from machine import Pin

import ir_codes

SAMSUNG_TV_REMOTE = 0
SAMSUNG_SB_REMOTE = 1


class IrReceiver:

    def __init__(self, remote_type: ir_codes.Remote, power_pin=0, ir_pin=22):

        super().__init__()
        self._call_back = None

        self._remote = remote_type

        power = Pin(power_pin, Pin.OUT)
        power.on()

        self._ir_rec = Pin(ir_pin, Pin.IN, Pin.PULL_UP)

    def _rec(self):
        # print("starting recording")
        pulses = []
        d = machine.time_pulse_us(self._ir_rec, 1, 2000)
        while d > 0:
            pulses.append(d)
            d = machine.time_pulse_us(self._ir_rec, 1, 5000)
        if not pulses:
            return False
        # print(pulses)
        out_bin = ""
        for us in pulses:
            if us > 2000:
                pass
            elif us < 1000:
                out_bin += "0"
            else:
                out_bin += "1"

        code = int(out_bin, 2)
        # print(out_bin)
        # print('Key {} -> {}'.format(str(hex(code)), self.codes.get(code)))
        return code, self._remote.decode(code)

    def _schedule_rec(self, _):
        state = machine.disable_irq()
        data = self._rec()
        machine.enable_irq(state)
        if data:
            self._call_back(data)

    def set_interrupt(self, cb):
        # print("setting interrupt...")
        self._call_back = cb
        self._ir_rec.irq(trigger=Pin.IRQ_FALLING, handler=self._schedule_rec)
        # print("interrupt set")

    def stop_interrupt(self):
        self._ir_rec.irq()
        self._call_back = None
