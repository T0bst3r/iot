from machine import Pin, TouchPad

import ir2
import ir_codes
from smart_light import Client, Light


def print_remote_code(data):
    # print("received data")
    # print(data)
    # print(data[1][1])
    #    print('Key {:#010X} -> {}'.format(data[0], data[1]))

    if data[1][1] == ir_codes.COLOR_GREEN:
        green_light.toggle()
    elif data[1][1] == ir_codes.VOLUME_UP:
        green_light.turn_on()
    elif data[1][1] == ir_codes.VOLUME_DOWN:
        green_light.turn_off()

    elif data[1][1] == ir_codes.COLOR_RED:
        red_light.toggle()
    elif data[1][1] == ir_codes.CHANNEL_UP:
        red_light.turn_on()
    elif data[1][1] == ir_codes.CHANNEL_DOWN:
        red_light.turn_off()


receiver = ir2.IrReceiver(remote_type=ir_codes.SAMSUNG_TV_REMOTE)
receiver.set_interrupt(print_remote_code)

client = Client('192.168.1.1')
green_light = Light(21, 'room1', client)
red_light = Light(23, 'room2', client)

green_touch = TouchPad(Pin(2))
red_touch = TouchPad(Pin(4))

green_sending = False
red_sending = False

while True:
    client.check_msg()

    if green_touch.read() < 300 and not green_sending:
        green_sending = True
        green_light.toggle()
    elif green_touch.read() >= 300:
        green_sending = False

    if red_touch.read() < 300 and not red_sending:
        red_sending = True
        red_light.toggle()
    elif red_touch.read() >= 300:
        red_sending = False
