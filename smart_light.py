# Complete project details at https://RandomNerdTutorials.com

import time

import machine
import ubinascii
from machine import Pin

from umqttsimple import MQTTClient


class Client:
    _instance = None
    # _client = None
    _client_id = ubinascii.hexlify(machine.unique_id())

    callbacks = None

    def __new__(cls, mqtt_server, restart_on_error=True):
        if cls._instance is None:
            cls.restart_on_error = restart_on_error
            cls._instance = super(Client, cls).__new__(cls)
            cls._client = MQTTClient(cls._client_id, mqtt_server)

        return cls._instance

    def _sub_cb(self, topic, message):
        # print((self, topic, message))
        # print(self.callbacks)
        self.callbacks[topic](message)

    def add_subscription(self, topic, callback):
        if self.callbacks is None:
            try:
                self.callbacks = {}
                self._client.set_callback(self._sub_cb)
                self._client.connect()
            except OSError as e:
                if self.restart_on_error:
                    restart_and_reconnect()
                else:
                    return e
        if type(topic) == str:
            topic = topic.encode()
        self.callbacks[topic] = callback
        self._client.subscribe(topic)

    def remove_subscription(self, topic):
        del self.callbacks[topic]

    def publish(self, topic, message, retain=False, qos=0):

        self._client.publish(topic, message, retain, qos)

    def check_msg(self):
        self._client.check_msg()


class Light:
    client = None

    def __init__(self, pin_nr: int, name: str, client: Client):
        self.light = Pin(pin_nr, Pin.OUT)
        if type(name) == bytes:
            self.name = name
        else:
            self.name = name.encode()
        self.sending = False
        self.client = client
        self.client.add_subscription(name, self._sub_cb)

    def _sub_cb(self, msg):

        if msg == b'on':
            self.light.on()
        elif msg == b'off':
            self.light.off()

    def turn_on(self):
        self.client.publish(self.name, b'on', retain=True)

    def turn_off(self):
        self.client.publish(self.name, b'off', retain=True)

    def toggle(self, val=None):
        if val is None:
            if self.is_on():
                self.turn_off()
            else:
                self.turn_on()
        elif val:
            self.turn_on()
        else:
            self.turn_off()

    def is_on(self):
        return self.light.value()


def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()
